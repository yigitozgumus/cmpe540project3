
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"Util.h"
#include"LogicAgent.h"
using namespace std;

Environment *env;


void parseCmdLine (int argc, char *argv[], 
				char *layoutFileName, 
				char *searchType, 
				int &nSteps, int &debug, int  &msec);

int main(int argc, char *argv[]){
		
		// parse
		system("clear");
		char layoutFileName[100]; char searchType[100]; int  msec, debug, nSteps;
		parseCmdLine(argc, argv,layoutFileName,searchType,debug,nSteps,msec);
		if (nSteps==-1) nSteps = INF;
		if (debug==0) Util::DEBUG = false; else Util::DEBUG = true;

		env = new Environment (layoutFileName);
		LogicAgent *agent = new LogicAgent (searchType);
		agent->initKB();      // get pos/dir from env and store them in current.pl
		for (int t=1;t<=nSteps;t++){
				env->print();
				agent->perceive ();
				agent->tellKB (t);
				agent->askKB (t); 
				agent->act(t);
				env->wait(msec);
		}
		env->print();
}


void printUsage()
{
		cout << "Usage:" << endl;
		cout << "./p3 -debug <0/1> -steps <nStepPlay> -search <random/prolog> -layout <layoutFile> -update <msec>" << endl;
		cout << "-debug to print out the commands " << endl;
		cout << "-steps for playing (-1 for play until end) " << endl;
		cout << "-search for prolog or self search " << endl;
		cout << "-layout for layout file" << endl;
		cout << "-update for update speed in printing the solution path (no printing if -1) " << endl;
		Util::Exit("Wrong command line arguments");
}


void parseCmdLine (int argc, char *argv[], 
				char *layoutFileName, 
				char *searchType,
				int  &debug,
				int  &nSteps,
				int  &msec)
{
		char evalType[100];
		if (argc!=11
						|| strcmp(argv[1],"-debug")
						|| strcmp(argv[3],"-steps")
						|| strcmp(argv[5],"-search")
						|| strcmp(argv[7],"-layout") 
						|| strcmp(argv[9],"-update")
						){
				printUsage();
		}

		debug   = atoi(        argv[2]);
		nSteps = atoi(        argv[4]);
		strcpy(searchType,    argv[6]);
		strcpy(layoutFileName,argv[8]);
		msec   = atoi(        argv[10]);

		if (strcmp(searchType,"random") && strcmp(searchType,"prolog")){
				Util::Exit("Wrong search type!");
				printUsage();
		}


}
