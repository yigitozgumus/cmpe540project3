#include"LogicAgent.h"
#include"Util.h"

using namespace std;
extern Environment *env;

// creates the agent, sets the search algorithm of the agent
// do not change
LogicAgent::LogicAgent (char *searchStr){
		if (!strcmp(searchStr,"random"))
				searchType = SEARCH_RANDOM;
		else if (!strcmp(searchStr,"prolog"))
				searchType = SEARCH_PROLOG;
		nSteps = 0;

		allKBFile     = "KB/all.pl";
		curKBFile     = "KB/current.pl";
		perceptKBFile = "KB/percepts.pl";
		pActKBFile    = "KB/potential-act.pl";
}


// agent perceives the current environment
void LogicAgent::perceive (){
		percept = env->perceive ();
}

void LogicAgent::initKB (){
		env->getInitAgentRowColDir(row, col, dir); 
		FILE *fp=fopen(curKBFile .c_str(),"w");  
		fprintf(fp,"location(1,%d,%d).\n",row,col);
		fprintf(fp,"dir(1,%s).\n",env->dirNames[dir]);
		fclose(fp);
		fp=fopen(perceptKBFile.c_str(),"w");
		fclose(fp);
}

// tell the current state to KB
// clear percept after telling
void LogicAgent::tellKB (int t){
		string perceptSentences="";
		if (percept.isFoodSmell)
				perceptSentences += "foodSmell("+INT2STR(t)+").\n";
		if (percept.isEnemySmell)
				perceptSentences += "enemySmell("+INT2STR(t)+").\n";
		if (percept.isEnemySight)
				perceptSentences += "enemySight("+INT2STR(t)+").\n";
		if (percept.isPitBreeze)
				perceptSentences += "pitBreeze("+INT2STR(t)+").\n";
		if (percept.isBumped)
				perceptSentences += "bump("+INT2STR(t)+").\n";
		if (percept.isFull)
				perceptSentences += "full("+INT2STR(t)+").\n";

		FILE *fp=fopen(perceptKBFile.c_str(),"a");  // add the percept
		fprintf(fp,"%s",perceptSentences.c_str());
		cout << "newPercept:" << perceptSentences << endl;
		fclose(fp);

		updateKB();
		percept.reset();
}

void LogicAgent::prologAct (int t, int a){
		cout << "ACT action:" << env->actionNames[a] << endl;
		FILE *fp=fopen(curKBFile.c_str(), "a");
		fprintf(fp,"action(%d,%s).\n", t, env->actionNames[a]);
		fclose(fp);

		int dR[5]={+0,-1,+1,+0,+0};
		int dC[5]={+0,+0,+0,-1,+1};
		string query;
		int effect;
		// if this potential action brings agent to a cell with food, choose it
		for (int i=0;i<5;i++){
				int newRow=row+dR[i];
				int newCol=col+dC[i];
				string rowCol=INT2STR(newRow)+","+INT2STR(newCol);
				query = "location("+INT2STR(t+1)+","+rowCol+").";
				if (callProlog(allKBFile,query)){
						FILE *fp=fopen(curKBFile.c_str(), "a");
						fprintf(fp,"%s\n", query.c_str());
						fclose(fp);
						row = newRow;
						col = newCol;
				}
		}
}


char outcomeAxiomNames[N_OUTCOMES][20]= 
 {"isClear", "hasFood", "hasNotFood", "hasEnemy", "hasNotEnemy",
		"hasPit", "hasNotPit", "hasDeadEnemy", "isWall"};

// checks out the outcomes on the predicted action/location 
vector<int> LogicAgent::prologSimEffect (int t, int a){
		cout << endl << "action:" << env->actionNames[a] << endl;
		FILE *fp=fopen(pActKBFile.c_str(), "w");
		fprintf(fp,"action(%d,%s).\n", t, env->actionNames[a]);
		fclose(fp);
		int dR[5]={+0,-1,+1,+0,+0};
		int dC[5]={+0,+0,+0,-1,+1};
		string query;
		vector<int> effect;
		// if this potential action brings agent to a cell with food, choose it
		for (int i=0;i<5;i++){
				int newRow=row+dR[i];
				int newCol=col+dC[i];
				string rowCol=INT2STR(newRow)+","+INT2STR(newCol);
				query = "location("+INT2STR(t+1)+","+rowCol+").";
				if (callProlog(allKBFile,query)){
						for (int o=0;o<N_OUTCOMES;o++){
								string query;
								query = string(outcomeAxiomNames[o])+"("+INT2STR(t+1)+","+rowCol+").";
								fp=fopen(pActKBFile.c_str(), "w"); fclose(fp);
								if (callProlog(allKBFile,query))
										effect.push_back(o);
						}
						// we don't know if it is food or enemy or wall
						if (effect.size()==0)
								effect.push_back(UNKNOWN);
						break;
				}
		}
		// reset potentialAct KB file as callProlog includes this file in default
		for (int o=0;o<effect.size();o++)
				cout << "effect:" << outcomeAxiomNames[effect[o]] << endl;
		return effect;
}

bool effectExists(vector<int> effects, int target){
		if (find (effects.begin(), effects.end(), target) == effects.end())
				return false;
		else
				return true;
}

// calls the corresponding search functions
void LogicAgent::askKB (int t){
		if (searchType==SEARCH_RANDOM){
				randomSearch();
		}
		else{
				string query;
				selectedAction=-1;
				vector< vector<int> > effects;
				for (int a=0;a<N_MAZE_ACTIONS;a++){
						effects.push_back(prologSimEffect (t, a));
				}
				// if action is KNOWN to be a good move (consuming the food), select
				for (int a=0;a<N_MAZE_ACTIONS && selectedAction==-1;a++){
						if (a==EAT && effectExists(effects[a], HIT_FOOD))
								selectedAction = a;
				}
				// if action is KNOWN to be a good move (hitting food), select
				for (int a=0;a<N_MAZE_ACTIONS && selectedAction==-1;a++){
						if (effectExists(effects[a], HIT_FOOD))
								selectedAction = a;
				}
				// if action is KNOWN to be a good move (attacking to an alive enemy), select
				for (int a=0;a<N_MAZE_ACTIONS && selectedAction==-1;a++){
						if (a==ATTACK && effectExists(effects[a],HIT_ENEMY) 
										          && !effectExists(effects[a],HIT_DEAD))
								selectedAction = a;
				}
				// if action is KNOWN to be an ok move (not hitting enemy or pit), select
				for (int a=0;a<N_MAZE_ACTIONS && selectedAction==-1;a++)
						if (   !effectExists(effects[a],HIT_WALL)
										&& effectExists(effects[a],HIT_NO_ENEMY)
										&& effectExists(effects[a],HIT_NO_PIT))
								selectedAction = a;
				// if action is KNOWN to be not a bad move, select
				for (int a=0;a<N_MAZE_ACTIONS && selectedAction==-1;a++)
						if (effectExists(effects[a], HIT_CLEAR))
								selectedAction = a;

				// if no idea, select forward
				if (selectedAction==-1) 
						selectedAction = FORWARD;

		}
		cout << "SELECTED ACTION: " << env->actionNames[selectedAction] << endl;
}

// query q is true if result of the following swipl command is 0
// return false if result is 1
bool LogicAgent::callProlog (string KB, string query){
		updateKB();
		usleep(1000);
		int res;
		string cmd = "swipl -s "+KB+" -t '"+query+"' 2>KB/out1; echo $? > KB/result";
		if (Util::DEBUG)
				cout << cmd << endl;
		int status = Util::callCmdWaitSec(cmd, 2);
		if (status!=0){
				system("reset");
				cout << "swipl -s "+KB+" -t '"+query+"'; echo $? " << endl;
				cout << "The command above was not successful" << endl;
				cout << "Something is wrong with your rules in rules.pl" << endl;
				cout << "Run the command yourself and see the reason. " << endl;
				Util::Exit("exiting...");
		}
		else{
				//cout << "The command above WAS successful, run yourself and see the reason. " << endl;
		}
		usleep(1000);
		FILE *fp=fopen("KB/result","r");
		if (fscanf(fp,"%d",&res)==EOF){
				cout << "swipl -s "+KB+" -t '"+query+"' ; echo $?" << endl;
		}
		fclose(fp);
		return (res==0)?(true):(false);
}

// combine rules, init state and percepts
void LogicAgent::updateKB(){
		string rulesKB   = "KB/rules.pl";
		string cmd = "cat "+rulesKB+" "+curKBFile +" "+perceptKBFile+" "+pActKBFile+" >"+allKBFile;
		system(cmd.c_str());
}


void LogicAgent::act (int t){
	 env->act (selectedAction, percept); // update percept if bumped
		tellKB(t+1);
		prologAct(t, selectedAction);
}




// assigned selectedAction to one of the possible actions
void LogicAgent::randomSearch(){
		selectedAction = rand()%N_MAZE_ACTIONS;
}


