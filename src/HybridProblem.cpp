#include"HybridProblem.h"
using namespace std;
extern Environment *env;
#define INV_TIMER (40)



/******************************* HybridProblem class **********************/
char Environment::mazeItems[N_MAZE_ITEMS]={'%','A','.',' ','i','E','P'};
char Environment::actionNames[N_MAZE_ACTIONS][20] = 
{"forward", "counterClockWise", "clockWise","attack","eat"};
char Environment::dirNames[N_DIRS][20] = {"north","east","south","west"};

/**
	* Creates a new environment
	* Reads the maze from file
	* Initializes the maze data
	**/
Environment::Environment(char *layoutFileName){
		nCols=nRows=-1;
		agentDir  = DIR_EAST;
		readFromFile (layoutFileName);
		eyeSightDist = 5;
}


Percept Environment::perceive(){
		Percept percept;
		// check if any food in the neighbourhood
		for (int i=0;i<foodPosList.size();i++){
				int foodPos = foodPosList[i];
				if (areNeighbours(foodPos, agentPos)){
						percept.isFoodSmell = true;
						cout << "Smell of food" << endl;
						break;
				}
		}
		// check if any enemy in the neighbourhood
		for (int i=0;i<enemyPosList.size();i++){
				if (isEnemyKilled[i]) continue;
				int enemyPos = enemyPosList[i];
				if (areNeighbours(enemyPos, agentPos)){
						cout << "Smell of enemy" << endl;
						percept.isEnemySmell = true;
						break;
				}
		}
		// check if any pits in the neighbourhood
		for (int i=0;i<pitPosList.size();i++){
				int pitPos = pitPosList[i];
				if (areNeighbours(pitPos, agentPos)){
						cout << "Breeze of pit" << endl;
						percept.isPitBreeze = true;
						break;
				}
		}
		// check if any enemy is in sight
		for (int i=0;i<enemyPosList.size();i++){
				int enemyPos = enemyPosList[i];
				if (isAgentInSightOfAgentWithDir(enemyPos, agentPos, agentDir)){
						percept.isEnemySight = true;
						cout << "Sight of enemy" << endl;
						break;
				}
		}
		return percept;
}

void Environment::getInitAgentRowColDir(int &row, int &col, int &dir){
		getRowColFromPos(row,col,initAgentPos);
		dir = initAgentDir;
		return;
}

// eat the food in the current cell
void Environment::eat(){
		agentFull=false;
		for (int f=0;f<foodPosList.size();f++){
				if (agentPos==foodPosList[f]){
						foodPosList.erase(foodPosList.begin()+f);
						agentFull=true;
						return;
				}
		}
}

// move forward attacking and killing the enemy (if it exists)
void Environment::attack(){
		int agentR, agentC;
		getRowColFromPos (agentR, agentC, agentPos);
		int dR[4]={-1,+0,+1,+0};
		int dC[4]={+0,+1,+0,-1};
		for (int e=0;e<enemyPosList.size();e++){
				int enemyR, enemyC;
				getRowColFromPos (enemyR, enemyC, enemyPosList[e]);
				for (int d=0;d<N_DIRS;d++){
						if (agentDir==d && (enemyR-agentR==dR[d]) && (enemyC-agentC==dC[d])){
								isEnemyKilled[e]=true; return;
						}
				}
		}
}
// index 0: good guy
// ow     : bad guys
/**
	* Agent with the corresponding index acts
	*/
int Environment::act (int action, Percept &percept){

		if (action==EAT){
				eat();
				percept.isFull = agentFull;
				return 0;
		}
		if (action==ATTACK)
				attack();
		updateAgentPosDir (action); 
		percept.isBumped = agentBumped;
		return 0;
}


/**
	* Returns the next state
	* Given the action and the current state 
	* If agent hits to the wall, return 0
	*/
void Environment::updateAgentPosDir (int action){
		int curPos = agentPos;
		int dC, dR, curC, curR, nextC, nextR;
		getRowColFromPos (curR, curC, curPos);
		getActionEffectRC (dR, dC, action);
		nextR = curR+dR;
		nextC = curC+dC;
		if (maze[nextR][nextC] == WALL){
				agentBumped = true;
				nextR = curR;
				nextC = curC;
		} else{
				agentBumped = false;
		}

		int newDir = agentDir;
		if (action == TURN_CW || action == TURN_CCW){
				int d=0;
				if (action == TURN_CW) d=1;
				else d=-1;
				newDir = (newDir + d);
				if (newDir<0) newDir+=N_DIRS;
				newDir = newDir%N_DIRS;
		}

		agentDir = newDir;
		agentPos = getPosFromRowCol (nextR, nextC);
		
		return;
}



void Environment::wait(int msec) {
		usleep(msec*1000); 
		system("clear");
}
/** 
	* Print out the maze
	*/
void Environment::print() {
		int agentC, agentR;
		getRowColFromPos (agentR, agentC, agentPos);
		for (int r=0;r<nRows;r++){
				for (int c=0;c<nCols;c++){
						int pos = getPosFromRowCol (r,c);

						bool isAssigned=false;
						for (int e=0;e<enemyPosList.size();e++){
								if (pos == enemyPosList[e]){
										if (!isEnemyKilled[e]){
												isAssigned = true;
												cout << FRED("E") ;
												break;
										}
								}
						}
						if (isAssigned) continue;

						if (agentPos == pos){
								if (agentDir==DIR_NORTH)
										cout << FBLU("^") ;
								else if (agentDir==DIR_SOUTH)
										cout << FBLU("v") ;
								else if (agentDir==DIR_EAST)
										cout << FBLU(">") ;
								else if (agentDir==DIR_WEST)
										cout << FBLU("<") ;
								continue;
						}

						for (int e=0;e<enemyPosList.size();e++){
								if (pos == enemyPosList[e]){
										if (isEnemyKilled[e])
												isAssigned = true;
										cout << FGRN("E") ;
								}
								break;
						}
						if (isAssigned) continue;

						if (maze[r][c]==WALL){
								cout << "%" ;
								continue;
						}
						if (maze[r][c]==PIT){
								cout << FRED("P");
								continue;
						}
						if (maze[r][c]==FOOD){
								for (int i=0;i<foodPosList.size();i++){
										if (foodPosList[i]==pos){
														cout << FGRN(".");
												isAssigned = true;
												break;
										}
								}
						}
						if (isAssigned) continue;


						cout << " ";
				}
				cout << endl;
		}
}

/**
	* Read the maze from file
	* %: wall
	* A: Our agent (good guy)
	* E: Enemy agents (bad guys)
	* .: Food
	* i: Invisibility cloak
	*/
void Environment::readFromFile (char fileName[]){
		FILE *fp = fopen (fileName,"r");
		if (fp==0){
				Util::Exit("Cannot open maze file for reading");
		}
		char s;
		int c;
		nRows=c=0;
		while (fscanf(fp,"%c",&s)!=EOF){
				if (s=='<' || s=='>' || s=='^' || s=='v'){
						agentPos = getPosFromRowCol (nRows, c);
						initAgentPos = agentPos;
						maze[nRows][c++]=FREE; 
						switch (s){
								case '<': agentDir = DIR_WEST;  break;
								case '>': agentDir = DIR_EAST;  break;
								case '^': agentDir = DIR_NORTH; break;
								case 'v': agentDir = DIR_SOUTH; break;
						}
						initAgentDir = agentDir;
				}else{
						switch (s){
								case '%':
										wallPosList.push_back (getPosFromRowCol (nRows, c));
										maze[nRows][c++]=WALL;
										break;
								case 'P':
										pitPosList.push_back(getPosFromRowCol (nRows, c));
										maze[nRows][c++]=PIT; 
										break;
								case 'E':
										enemyPosList.push_back(getPosFromRowCol (nRows, c));
										isEnemyKilled.push_back(false);
										maze[nRows][c++]=ENEMY; 
										break;
								case '.':
										foodPosList.push_back (getPosFromRowCol (nRows, c));
										maze[nRows][c++]=FOOD;
										break;
								case ' ':
										maze[nRows][c++]=FREE;
										break;
								case '\n':
										if (nCols==-1)
												nCols = c;
										else if (nCols != c){
												Util::Exit("Environment in maze file in dimensions");
										}
										c=0; 
										nRows++;
										break;
								default: 
										std::cout << s << std::endl;
										Util::Exit("Environment in maze file in characters");
						}
				}
		}
		if (c!=0)
				nRows++;
}

/**
	* Utility function that returns 1d index from 2d position
	*/
int  Environment::getPosFromRowCol (int r, int c){
		return (r*nCols + c);
}

/** 
	* Utility function that returns 2d position from 1d index
	*/
void Environment::getRowColFromPos (int &r, int &c, int index){
		c = index%nCols;
		r = floor(1.0*index/nCols);
}

/**
	* Returns the change in 2d position with an action
	*/
void Environment::getActionEffectRC (int &dr, int &dc, int action){
		dr=dc=0;
		if (action==FORWARD || action==ATTACK){
				switch (agentDir){
						case (DIR_NORTH): dr = -1; break;
						case (DIR_SOUTH): dr = +1; break;
						case (DIR_WEST):  dc = -1; break;
						case (DIR_EAST):  dc = +1; break;
				}
		}
}

bool Environment::areNeighbours (int pos1, int pos2){
		int r1,c1;
		int r2,c2;
		getRowColFromPos (r1, c1, pos1);
		getRowColFromPos (r2, c2, pos2);
		if (   (abs(r1-r2)==1 && (abs(c1-c2)==0))
						|| (abs(r1-r2)==0 && (abs(c1-c2)==1)))
				return true;
		else
				return false;
}

/**
	* Returns true if it is in eye-sight (no wall in between) and 
	* within eyeSightDist distance
	* dir order is north,east,south, west
	*/
bool Environment::isAgentInSightOfAgentWithDir(int targetPos, int originPos, int originDir){
		static int deltaR[4]={-1,+0,+1,+0};
		static int deltaC[4]={+0,+1,+0,-1};
		int targetR, targetC;
		int originR, originC;
		getRowColFromPos (targetR, targetC, targetPos);
		getRowColFromPos (originR, originC, originPos);
		// check out if in the line of eye sight
		if (((originDir==DIR_NORTH || originDir==DIR_SOUTH) && (targetC==originC))
						|| 
		    ((originDir==DIR_WEST  || originDir==DIR_EAST) &&  (targetR==originR))){
				// check out if there is any wall in between
				for (int i=1; i<=eyeSightDist; i++){
						int newR = originR+i*deltaR[originDir];
						int newC = originC+i*deltaC[originDir];
						if (maze[newR][newC]==WALL){      // if there is wall in between
								//cout << "in the eyesight but there is wall, return false" << endl;
								return false;
						}
						int newPos = getPosFromRowCol (newR, newC);
						if (newPos == targetPos){        // if targetPos reached within eyeSightDist
								//cout << "in the eyesight so visible, return true" << endl;
								return true;
						}
				}
				//cout << "in the eye direction but far away, return false" << endl;
				return false;
		}else{
				//cout << "NOT in the eyesight, return false" << endl;
				return false;
		}
}
