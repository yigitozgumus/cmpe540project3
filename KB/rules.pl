location(T1,R,C) :-
  T0  is T1 - 1,
  RN  is R - 1,
  RS  is R + 1,
  CW  is C - 1,
  CE  is C + 1,
  (
    ((action(T0,eat);action(T0,clockWise);action(T0,counterClockWise)), location(T0,R,C));
    ((action(T0,attack);action(T0,forward)), bump(T1), location(T0,R,C));
    ((action(T0,attack);action(T0,forward)), dir(T0,north), not(bump(T1)), location(T0,RS,C));
    ((action(T0,attack);action(T0,forward)), dir(T0,south), not(bump(T1)), location(T0,RN,C));
    ((action(T0,attack);action(T0,forward)), dir(T0,west),  not(bump(T1)), location(T0,R,CE));
    ((action(T0,attack);action(T0,forward)), dir(T0,east),  not(bump(T1)), location(T0,R,CW))
  ).

dir(T1,north) :-
  T0 is T1 - 1,
    (
        ((action(T0,eat);action(T0,attack);action(T0,forward)), dir(T0,north) );
        (action(T0,clockWise)       , dir(T0,west));
        (action(T0,counterClockWise), dir(T0,east))
    ).

dir(T1,east) :-
  T0 is T1 - 1,
    (
        ((action(T0,eat);action(T0,attack);action(T0,forward)), dir(T0,east));
        (action(T0,clockWise)       , dir(T0,north));
        (action(T0,counterClockWise), dir(T0,south))
    ).

dir(T1,south) :-
  T0 is T1 - 1,
    (
        ((action(T0,eat);action(T0,attack);action(T0,forward)), dir(T0,south));
        (action(T0,clockWise)       , dir(T0,east));
        (action(T0,counterClockWise), dir(T0,west))
    ).

dir(T1,west) :-
  T0 is T1 - 1,
    (
        ((action(T0,eat);action(T0,attack);action(T0,forward)), dir(T0,west) );
        (action(T0,clockWise)       , dir(T0,south));
        (action(T0,counterClockWise), dir(T0,north))
    ).


/* Wall variables related */
isWall(T,R,C) :-
    isWall(R,C).

isWall(X,Y):-
  (X =:= 0;
   Y =:= 0);
  (
   bump(T),
   not(T is -1),
   T0 is T - 1,
   ((dir(T0,east),YW is Y - 1,location(T0,X,YW),location(T,X,YW));
    (dir(T0,south),XN is X - 1,location(T0,XN,Y),location(T,XN,Y));
    (dir(T0,north),XS is X + 1,location(T0,XS,Y),location(T,XS,Y));
    (dir(T0,west),YE is Y + 1,location(T0,X,YE),location(T,X,YE)))).


isClear(T,R,C) :-     % change in Q1
    hasNotPit(T,R,C),
    not(isWall(R,C)),
    hasNotEnemy(T,R,C).

%Initial Definitions
bump(-1).
pitBreeze(-1).
enemySmell(-1).
enemySight(-1).
foodSmell(-1).

hasNotEnemy(T,X,Y):-
  M is T - 1,
  T0 is T - 2,
   (enemySmell(M) -> checkLocations(M,T0,X,Y) ; true).

hasNotPit(T,X,Y):-
  M is T -1,
  T0 is T-2,
  (pitBreeze(M) -> (checkLocations(M,T0,X,Y)) ; true).

checkCellForBreeze(M,X,Y):-
  not(M is 1),
  Z is M -1,
  checkCellForBreeze(Z,X,Y);
  (
  (location(M,X,Y) -> pitBreeze(M);false)
  ).

checkBreeze(M,X,Y):-
  S is X + 1,
  N is X - 1,
  E is Y + 1,
  W is Y - 1,
  (
  (location(A,S,Y) -> not(pitBreeze(A));false ),
  (location(B,N,Y) -> not(pitBreeze(B));false),
  (location(C,X,E) -> not(pitBreeze(C));false),
  (location(D,X,W) -> not(pitBreeze(D));false)
  ).

checkLocations(M,T0,X,Y):-
  (
  not((dir(T0,north), S is X + 1 , S1 is X+2 , location(M,S,Y) , location(T0,S1,Y),!) ),
  not((dir(T0,south) , N is X - 1 , N1 is X - 2 , location(M,N,Y), location(T0,N1,Y),!) ),
  not((dir(T0,west) , E is Y + 1 , E1 is Y + 2 , location(M,X,E), location(T0,X,E1),!) ),
  not((dir(T0,east) , W is Y - 1 , W1 is Y - 2 , location(M,X,W), location(T0,X,W1),!) )
  ).

  checkTurns(M,T0,X,Y):-
    (
    (dir(M,north), S is X + 1 , location(M,S,Y) ,(dir(T0,east);dir(T0,west)), !);
    (dir(M,south) , N is X - 1 , location(M,N,Y) ,(dir(T0,east);dir(T0,west)), !);
    (dir(M,west) , E is Y + 1 , location(M,X,E) ,(dir(T0,north);dir(T0,south)), !);
    (dir(M,east) , W is Y - 1 , location(M,X,W) ,(dir(T0,north);dir(T0,south)), !)
    )
    .


hasPit(T,X,Y):-       % change in Q3
not(hasNotPit(T,X,Y)).
hasEnemy(T,X,Y):-  % change in Q3
 M is T - 1,
  T0 is T - 2,
   ((enemySmell(M),enemySight(M)) -> checkLocations(M,T0,X,Y) ; false).
hasDeadEnemy(T,X,Y):-% change in Q3
  M is T - 1,
  ((enemySmell(M1),
   enemySight(M1),
   not(M1 is -1),
   T01 is M1 - 1,
    M1 < M) -> checkLocations(M1,T01,X,Y); false).

hasFood(-1,X,Y).     % change in Q4
hasNotFood(-1,X,Y).    % change in Q4

isWall(-1,-1).
isWall(-1,-1,-1).
