location(T1,R,C) :-
  T0  is T1 - 1,
  RN  is R - 1,
  RS  is R + 1,
  CW  is C - 1,
  CE  is C + 1,
  (
    ((action(T0,eat);action(T0,clockWise);action(T0,counterClockWise)), location(T0,R,C));
    ((action(T0,attack);action(T0,forward)), bump(T1), location(T0,R,C));
    ((action(T0,attack);action(T0,forward)), dir(T0,north), not(bump(T1)), location(T0,RS,C));
    ((action(T0,attack);action(T0,forward)), dir(T0,south), not(bump(T1)), location(T0,RN,C));
    ((action(T0,attack);action(T0,forward)), dir(T0,west),  not(bump(T1)), location(T0,R,CE));
    ((action(T0,attack);action(T0,forward)), dir(T0,east),  not(bump(T1)), location(T0,R,CW))
  ).

dir(T1,north) :-
  T0 is T1 - 1,
		(
				((action(T0,eat);action(T0,attack);action(T0,forward)), dir(T0,north) );
				(action(T0,clockWise)       , dir(T0,west));
				(action(T0,counterClockWise), dir(T0,east))
		).

dir(T1,east) :-
  T0 is T1 - 1,
		(
				((action(T0,eat);action(T0,attack);action(T0,forward)), dir(T0,east));
				(action(T0,clockWise)       , dir(T0,north));
				(action(T0,counterClockWise), dir(T0,south))
		).

dir(T1,south) :-
  T0 is T1 - 1,
		(
				((action(T0,eat);action(T0,attack);action(T0,forward)), dir(T0,south));
				(action(T0,clockWise)       , dir(T0,east));
				(action(T0,counterClockWise), dir(T0,west))
		).

dir(T1,west) :-
  T0 is T1 - 1,
		(
				((action(T0,eat);action(T0,attack);action(T0,forward)), dir(T0,west) );
				(action(T0,clockWise)       , dir(T0,south));
				(action(T0,counterClockWise), dir(T0,north))
		).


/* Wall variables related */
isWall(T,R,C) :- 
		isWall(R,C).
isWall(X,Y):-         % change in Q1
  (X =:= 0;
 	 Y =:= 0).
isClear(T,R,C) :-     % change in Q1
		hasNotEnemy(T,R,C), 
		hasNotPit(T,R,C),
		not(isWall(R,C)).

bump(-1).
hasNotEnemy(T,X,Y).   % change in Q2
hasNotPit(-1,X,Y).    % change in Q2

hasPit(-1,X,Y).       % change in Q3
hasEnemy(-1,X,Y).     % change in Q3
hasDeadEnemy(-1,X,Y). % change in Q3

hasFood(-1,X,Y).      % change in Q4
hasNotFood(T,X,Y).    % change in Q4

isWall(-1,-1).
isWall(-1,-1,-1).
location(1,1,1).
dir(1,east).
action(1,clockWise).
location(2,1,1).
action(2,eat).
location(3,1,1).
action(3,attack).
location(4,2,1).
action(4,attack).
location(5,3,1).
action(5,forward).
location(6,4,1).
enemySight(1).
pitBreeze(4).
