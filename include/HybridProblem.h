#ifndef HYBRIDPROBLEM_H
#define HYBRIDPROBLEM_H
#include"Util.h"
#include"LogicAgent.h"
#include <iostream>

#define FORWARD  0
#define TURN_CCW 1
#define TURN_CW  2
#define ATTACK   3
#define EAT      4
#define N_MAZE_ACTIONS 5
#define MAX_N_ACTIONS 5                       
#define MAX_MAP_SIZE 500

#define WALL    0
#define AGENT   1
#define FOOD    2
#define FREE    3
#define CLOAK   4
#define ENEMY   5
#define PIT     6
#define N_MAZE_ITEMS 7

#define DIR_NORTH 0
#define DIR_EAST  1
#define DIR_SOUTH 2
#define DIR_WEST  3
#define N_DIRS    4


#define XOR(x,y) ((!x && y) || (!y && x))

class Percept;

class Environment {
				std::vector<int>  foodPosList;      // list of food positions 
				std::vector<int>  wallPosList;      // list of wall positions
				std::vector<int>  enemyPosList;     // list of enemy positions 
				std::vector<bool> isEnemyKilled;    // life status of enemies
				std::vector<int>  pitPosList;       // list of pit positions 
				int  agentPos;                           // current agent position
				int  agentDir;                           // current agent direction
		public:
				Environment(char *fileName);
				Percept perceive();

				int  act (int action, Percept &percept);  // action and returned percept (bump or full)
				void attack();                            // particular attack action
				void eat();                               // particular eat action
				void updateAgentPosDir (int action);      // update the pos/dir, result of action


				/* Names of items of the maze and actions */
				static char mazeItems[N_MAZE_ITEMS];             // item codes for file input/output
				static char actionNames[N_MAZE_ACTIONS][20];     // names of the actions
				static char dirNames[N_DIRS][20];                // names of the directions

				/* Names of items of the maze and actions */
				void getInitAgentRowColDir(int &row, int &col, int &dir); // this is used to init pos/dir  in KB
				int  initAgentPos;                       // initial agent position
				int  initAgentDir;                       // initial agent direction
				bool agentBumped;                        // if agent bumped to the wall, result of prev. act
				bool agentFull;                          // if agent got full, result of prev. act (eat)


				// Maze related
				void print();                                         // draws out the maze
				void wait(int msec);                                  // waits to show the maze
				int  maze[MAX_MAP_SIZE][MAX_MAP_SIZE];                // items in the map (WALL/AGENT/FOOD/FREE)
				int  nCols;                                           // column number of the maze
				int  nRows;                                           // row number of the maze
				int  getPosFromRowCol (int r, int c);                 // map 2d index to 1d index
				void getRowColFromPos (int &r, int &c, int index);    // map 1d index to 2d index
				void getActionEffectRC (int &dr, int &dc, int action);// (delta) position change with action
				bool areNeighbours (int pos1, int pos2);              // 4-neighbourhood
				void readFromFile (char fileName[]);                  // reads the maze

				int  eyeSightDist;                                    // eyesight not important
				bool isAgentInSightOfAgentWithDir(int targetPos, int originPos, int originDir);

};

#endif 


