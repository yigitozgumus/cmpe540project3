#ifndef LOGICAGENT_H
#define LOGICAGENT_H
#include"HybridProblem.h" 
#include<stdlib.h>
#include<vector>
#include<queue>
#include<string>


#define SEARCH_RANDOM 0
#define SEARCH_PROLOG 1

#define HIT_CLEAR    0
#define HIT_FOOD     1
#define HIT_NO_FOOD  2
#define HIT_ENEMY    3
#define HIT_NO_ENEMY 4
#define HIT_PIT      5
#define HIT_NO_PIT   6
#define HIT_DEAD     7
#define HIT_WALL     8
#define N_OUTCOMES   9
#define UNKNOWN      9

#define SIMULATE_ACT 0
#define UPDATE_KB_WITH_ACT 1

#define INF (1000000)

class Percept{
		public:
				Percept(){
						reset();
				}
				void  reset(){
						isPitBreeze = isEnemySmell = isFoodSmell = isEnemySight = isBumped = isFull = false;
				}
				bool  isEnemySmell;
				bool  isFoodSmell;
				bool  isEnemySight;
				bool  isPitBreeze;
				bool  isBumped;
				bool  isFull;
};



class LogicAgent{
				int    searchType;
				int    nSteps;
		public:

				
				// These are implemented for you, but feel free to change
				LogicAgent (char *searchType);
				Percept percept;
				void  initKB ();
				void  tellKB (int t);
				void  updateKB ();
				void  askKB (int t);
				bool  callProlog (std::string KB, std::string q);

				int   row;
				int   col;
				int   dir;

				std::string allKBFile;
				std::string curKBFile;
				std::string perceptKBFile;
				std::string pActKBFile;

				std::vector<int> prologSimEffect (int t, int a);
				void prologAct (int t, int a);

				int   selectedAction;
				int   maxSearchDepth;

				void  perceive ();
				void  search ();
				void  act (int t);

				void  randomSearch();

};

#endif
