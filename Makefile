SRC=src/p3.cpp src/Util.cpp src/HybridProblem.cpp src/LogicAgent.cpp

INCLUDES=include/Util.h include/HybridProblem.h include/LogicAgent.h

CC=g++

CFLAGS=-g -Iinclude

p3: $(SRC) $(INCLUDES)
ifeq ($(NO_COLOR),1)
	$(CC) -D NO_COLORED_OUTPUT $(CFLAGS) -o p3 $(SRC)
else
	$(CC) $(CFLAGS) -o p3 $(SRC)
endif 

clean:
	rm p3
